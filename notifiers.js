module.exports = {
  notifiers: {
    slack: {
      url: ''
    },
    discord: {
      url: ''
    }
  },
  exec: function (template = 'default', options = {}) {
    const engines = {
      slack: require('./slack'),
      discord: require('./discord'),
    }
    if (this.notifiers) {
      Object.keys(this.notifiers).forEach((engine) => {
        if (this.notifiers[engine] && this.notifiers[engine].url && engines[engine] && engines[engine].templates) {
          engines[engine].url = this.notifiers[engine].url
          const worker = engines[engine].templates[template].bind(engines[engine]);
          worker(options);
        }
      })
    }
  }
}