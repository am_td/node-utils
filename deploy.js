const os = require("os");
const fs = require("fs");
const exec = require("child_process").execSync;
const rsync = require("rsyncwrapper");
// NOTE: Loads package.json relative to running process
const package = require(process.cwd() + "/package.json");

module.exports = {
  deploy: function(stage) {
    /* EXAMPLE package.json:
    {
      "name": "app",
      "config": {
        "deploy": {
          "stages": {
            "test": {
              "branch": "gitbranch",
              "type": "rsync|aws",
              "address": "somewhere.com",
              "port": "22",
              "user": "appuser",
              "deploy_to": "/srv/app",
              "aws_profile": "default",
              "aws_endpoint_url": "https://storage.yandexcloud.net",
              "aws_bucket_name": "super-bucket-name",
              "public_url": "https://app.public",
              "notifiers": {
                "slack": { "url": "https://hooks.slack.com..." },
                "discord": { "url": "https://discordapp.com/channels/..." }
              }
            }
          }
        }
      }
    }
    */

    const process_start_at = new Date();
    const git_revision = exec("git rev-parse --short HEAD")
      .toString()
      .trim();
    const git_branch = exec("git rev-parse --abbrev-ref HEAD")
      .toString()
      .trim();
    const deployer = `${os.userInfo().username}@${os.hostname()}`;

    // const stage = process.argv[2]
    const config = package.config.deploy.stages[stage];
    if (!config) {
      console.error(`\x1b[31mEnvironment ${stage} not found!`);
      return 1;
    }

    if (config.branch && config.branch != git_branch) {
      console.error(
        `Wrong current branch, got ${git_branch} must be ${config.branch}!`
      );
      return 3;
    }

    const notifiers = require("./notifiers");
    notifiers.notifiers = config.notifiers;

    let build_path = package.config.deploy.build_path;
    if (!build_path) {
      console.error("Build path is empty!");
      return 2;
    }
    if (!build_path.match(/^.+\/\.?$/)) {
      build_path += "/";
    }

    const valid_masks = [
      /^index\.html$/,
      /^favicon\.ico$/,
      /^favicon\.png$/,
      /^assets$/,
      /^[0-9]+\.[a-z0-9]+\.js$/,
      /^polyfills\.[0-9a-z]+\.js$/,
      /^runtime\.[0-9a-z]+\.js$/,
      /^common\.[0-9a-z]+\.js$/,
      /^styles\.[0-9a-z]+\.css/,
      /^.+\.woff2?$/,
      /^.+\.ttf$/,
      /^.+\.svg$/
    ];
    const build_files = fs.readdirSync(build_path);
    const valid_build_files = build_files.filter(name => {
      return !!valid_masks.find(re => name.match(re));
    });

    if (valid_build_files.length / build_files.length < 0.5) {
      console.error(`Build path ${build_path} maybe wrong.`);
      // return 3
    }

    function notify(error = undefined) {
      const process_finish_at = new Date();
      const process_run_delta = (process_finish_at - process_start_at) / 1000;
      notifiers.exec("deploy", {
        name: package.name,
        package_name: package.name,
        package_version: package.version,
        stage: stage,
        public_url: config.public_url,
        git_branch: git_branch,
        git_revision: git_revision,
        process_finish_at: process_finish_at,
        process_run_delta: process_run_delta,
        deployer: deployer,
        error: error
      });
    }

    switch (config.type) {
      case "aws":
        exec(
          [
            `aws`,
            `--profile=${config.aws_profile || "default"}`,
            `--endpoint-url=${config.aws_endpoint_url}`,
            `--delete`,
            `s3 sync ${build_path} s3://${config.aws_bucket_name}`
          ].join(" ")
        );
        break;
      default:
        rsync(
          {
            src: build_path,
            dest: `${config.user}@${config.address}:${config.deploy_to}`,
            port: `${config.port}`,
            ssh: true,
            recursive: true,
            delete: true,
            args: ["-acvzh"]
          },
          function(error, stdout, stderr, cmd) {
            const process_finish_at = new Date();

            console.log(cmd);
            if (error) {
              console.log(error.message);
              notify(error);
            } else {
              console.log(stdout);
              notify();
            }

            console.log(
              "Total execution time: %dms",
              process_finish_at - process_start_at
            );
          }
        );
        break;
    }
  },
  upload: function(stage, src_path) {
    if (typeof src_path === "string") {
      src_path = [src_path];
    }

    const process_start_at = new Date();
    const git_revision = exec("git rev-parse --short HEAD")
      .toString()
      .trim();
    const git_branch = exec("git rev-parse --abbrev-ref HEAD")
      .toString()
      .trim();
    const deployer = `${os.userInfo().username}@${os.hostname()}`;

    const config = package.config.deploy.stages[stage];
    if (!config) {
      console.error(`\x1b[31mEnvironment ${stage} not found!`);
      return 1;
    }

    const notifiers = require("./notifiers");
    notifiers.notifiers = config.notifiers;

    function notify(error = undefined) {
      const process_finish_at = new Date();
      const process_run_delta = (process_finish_at - process_start_at) / 1000;
      notifiers.exec("deploy", {
        name: package.name,
        package_name: package.name,
        package_version: package.version,
        stage: stage,
        public_url: config.public_url,
        git_branch: git_branch,
        git_revision: git_revision,
        process_finish_at: process_finish_at,
        process_run_delta: process_run_delta,
        deployer: deployer,
        error: error
      });
    }

    switch (config.type) {
      case "aws":
        exec(
          [
            `aws`,
            `--profile=${config.aws_profile || "default"}`,
            `--endpoint-url=${config.aws_endpoint_url}`,
            `--delete`,
            `s3 sync ${src_path} s3://${config.aws_bucket_name}`
          ].join(" ")
        );
        break;
      default:
        rsync(
          {
            src: src_path,
            dest: `${config.user}@${config.address}:${config.deploy_to}`,
            port: `${config.port}`,
            ssh: true,
            recursive: true,
            delete: true,
            args: ["-acvzh"]
          },
          function(error, stdout, stderr, cmd) {
            const process_finish_at = new Date();

            console.log(cmd);
            if (error) {
              console.log(error.message);
              notify(error);
            } else {
              console.log(stdout);
              notify();
            }

            console.log(
              "Total execution time: %dms",
              process_finish_at - process_start_at
            );
          }
        );
        break;
    }
  }
};
