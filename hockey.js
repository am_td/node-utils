module.exports = {
  options: {
    appId: process.env.HOCKEY_APPID,
    appApiKey: process.env.HOCKEY_APPKEY,
    releaseType: 'alpha',
    repositoryUrl: '',
    verbose: false
  },
  upload: function (filename, commitId = '') {
    const FormData = require('form-data')
    const fs = require('fs')

    const fileStream = fs.createReadStream(filename);
    fileStream.on('error', () => {
      console.log('Error reading binary file for upload to HockeyApp');
    });

    const form = new FormData();
    form.append('status', 2)
    form.append('notes', 'Automated release.')
    form.append('notes_type', 0)
    form.append('notify', 1)
    form.append('ipa', fileStream)
    // form.append('strategy', 'replace')
    form.append('release_type', {
      'alpha': 2,
      'beta': 0,
      'release': 1
    } [this.options.releaseType] || 0)
    //form.append('commit_sha', commitId)
    //form.append('repository_url', this.options.repositoryUrl)

    const API_HOST = 'rink.hockeyapp.net';
    const API_PATH = `/api/2/apps/${this.options.appId}/app_versions`;

    const req = form.submit({
      host: API_HOST,
      path: API_PATH,
      protocol: 'https:',
      headers: {
        'Accept': 'application/json',
        'X-HockeyAppToken': this.options.appApiKey
      }
    }, function (err, res) {
      if (err) {
        throw err
      }
      if (res.statusCode !== 200 && res.statusCode !== 201) {
        const error = `Upload failed: ${res.statusCode}, ${res.statusMessage}`
        throw error
      }
      // let jsonString = ''
      // res.on('data', (chunk) => {
      //   jsonString += String.fromCharCode.apply(null, new Uint16Array(chunk))
      // });
    });

    if (this.verbose) {
      const len = parseInt(req.getHeader('content-length'), 10);
      let cur = 0;
      const total = len / 1048576;
      req.on('data', (chunk) => {
        cur += chunk.length;
        console.log("Downloading " + (100.0 * cur / len).toFixed(2) + "%, Downloaded: " + (cur / 1048576).toFixed(2) + " mb, Total: " + total.toFixed(2) + " mb");
      });
    }
  }
}