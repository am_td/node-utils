module.exports = {
  url: '',
  notify: function (text = '', attachments = []) {
    if (!this.url) {
      console.error('No Slack Webhook URL provided')
      return
    }

    const options = {
      method: 'post',
      body: {
        text: text,
        attachments: attachments
      },
      json: true,
      url: this.url,
      headers: {}
    }

    const request = require('request')
    request(options, function (err, res, body) {
      if (err) {
        console.error(err)
      }
    });
  },
  templates: {
    deploy: function (options = {}) {
      /*
      options: {
        stage: ,
        public_url: ,
        package_name: ,
        package_version: ,
        git_branch: ,
        git_revision: ,
        process_finish_at: ,
        process_run_delta: ,
        deployer: ,
      }
      */
      const attachment = {
        title: `${options.package_name} ${options.stage}`,
        title_link: options.public_url,
        text: '',
        color: '',
        author_name: 'deploy.js',
        footer: '',
        footer_icon: `${options.public_url}/assets/images/logo.png`,
        ts: Math.floor(options.process_finish_at / 1000),
        fields: [{
            title: 'Stage',
            value: options.stage,
            short: true
          },
          {
            title: 'Version',
            value: options.package_version,
            short: true
          },
          {
            title: 'Branch',
            value: options.git_branch,
            short: true
          },
          {
            title: 'Revision',
            value: options.git_revision,
            short: true
          }
        ]
      };
      attachment.fields = attachment.fields.filter((field) => !!field.value);
      if (options.error) {
        attachment.text = `Deploy by ${options.deployer}: *failed* in ${options.process_run_delta}s.`
        attachment.color = 'danger'
      } else {
        attachment.text = `Deploy by ${options.deployer}: _successful_ in ${options.process_run_delta}s.`
        attachment.color = 'good'
      }
      return this.notify('', [attachment]);
    }
  }
}