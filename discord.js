module.exports = {
  url: '',
  notify: function (content = '', embeds = []) {
    if (!this.url) {
      console.error('No Discord Webhook URL provided')
      return
    }
    const options = {
      method: 'post',
      body: {
        content: content,
        embeds: embeds
      },
      json: true,
      url: this.url,
      headers: {}
    }
    const request = require('request')
    request(options, function (err, res, body) {
      if (err) {
        console.error(err)
      }
    });
  },
  templates: {
    deploy: function (options = {}) {
      const embed = {
        title: `${options.package_name} ${options.stage}`,
        url: options.public_url,
        description: '',
        // color: '',
        // timestamp: Math.floor(options.process_finish_at / 1000),
        footer: {
          text: 'deploy.js',
          icon_url: `${options.public_url}/assets/images/logo.png`
        },
        fields: [{
            name: 'Stage',
            value: options.stage,
            inline: true
          },
          {
            name: 'Version',
            value: options.package_version,
            inline: true
          },
          {
            name: 'Branch',
            value: options.git_branch,
            inline: true
          },
          {
            name: 'Revision',
            value: options.git_revision,
            inline: true
          }
        ]
      };
      embed.fields = embed.fields.filter((field) => !!field.value);
      if (options.error) {
        embed.description = `Deploy by ${options.deployer}: *failed* in ${options.process_run_delta}s.`
        // embed.color = 'danger'
      } else {
        embed.description = `Deploy by ${options.deployer}: _successful_ in ${options.process_run_delta}s.`
        // embed.color = 'good'
      }
      return this.notify('', [embed]);
    }
  }
}